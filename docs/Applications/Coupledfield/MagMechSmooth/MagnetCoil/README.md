# Magneto-mechanic Coupling with Grid Adaptation

This example shows how a grid adaptation with mesh smoothing PDE can be used in a coupled magneto-mechanic problem.
A copper coil with a steel core is placed in magnetic field of a permanent magnet. Due to a current flowing through the windings a Lorentz force acts on the coil and causes it to move in the direction of the magnet. 
The coil is considered a stranded coil and it is constrained by springs acting in y-direction. 
Only the rigid body movement of the coil is relevant, the mechanical deformations of the coil are not of interest.
The sketch of the considered problem is shown below.
 
![image](sketch.svg){: width="400px"}

This problem is considered in static and in transient case. 
In the static analysis a constant current flows through the coil. 
In the transient case an alternating current flows through the coil what leads to oscillations.
The force acting on the coil depends on the coil's position relative to the magnet, so to obtain accurate results for larger displacements the geometry needs to be updated as the simulation is performed. 
However, updating the geometry without a grid adaptation causes the simulation to crash due to mesh deformation. 
To avoid heavy mesh distortions and overlapping elements in the air around the moving coil a grid adaptation is necessary. 
In this case the moving mesh method is used, which improves the accuracy of the results and allows for larger displacements of the moving parts without remeshing. 
The air surrounding the coil is treated as pseudo-solid for which the smooth PDE is solved, with the displacements of the coil at the air-coil interfaces as inhomogeneous Dirichlet boundary conditions. 
This leads to smooth deformation over the whole air domain, instead of heavy deformation of only the first line of elements. 
The detailed explanation of the smooth PDE can be found [here](../../../../PDEExplanations/Singlefield/SmoothPDE/README.md).

## Mesh

The geometry for the simulation can be generated with coreform cubit using the journal file [MagnetCoil.jou](MagnetCoil.jou). 
The problem is set up as 2D but since the magnetic edge formulation is used a 3D mesh with one element in thickness direction is necessary. 
The geometry is based on the sketch but the symmetry was taken into account to reduce model size. 
The mesh used in the simulations is shown below.

![image](mesh.png){: width="600px"}

## Material

The material parameters are defined in the file [mat.xml](mat.xml). 
The parameters were chosen such that the behavior of the coil approximates rigid body movement and the deformation of the coil is not considered. 
The electric conductivity of all materials is set to zero so that no eddy currents are induced. 
The stranded coil is in reality made out of copper and isolating material which both have magnetic permeability close to the vacuum permeability $\mu_0$, so this value is assumed for the coil. The mechanical material properties(density, elasticity modulus and Poisson number) of copper are set for the coil. 
The core has mechanical properties of steel and vaccum magnetic permeability.
The use of the smooth PDE requires a definition of special smooth parameters(elasticity modulus and Poisson number) for the air. 

~~~
<material name="air">
  <smooth> 
    <elasticity>
      <linear>
        <isotropic>
          <elasticityModulus>
            <real> 1 </real>
          </elasticityModulus>
          <poissonNumber>
            <real> 0 </real>
          </poissonNumber>
        </isotropic>
      </linear>
    </elasticity>
  </smooth>
  ...
~~~

## Static analysis

The xml input file for the simulation is [MagnetCoil_smooth.xml](MagnetCoil_smooth.xml). 
The first sequence step is the static analysis. 
A second file with the same set-up but without grid adaptation is provided for reference: [MagnetCoil_ref.xml](MagnetCoil_ref.xml).

### Boundary conditions

The permanent magnet has prescribed magnetic flux density of $1.5T$ in negative y-direction and at the outer edges of the air domain the flux parallel boundary conditions are applied.

~~~
<magneticEdge>
...
  <bcsAndLoads>
    <fluxParallel name="S_Flux_x"/>
    <fluxParallel name="S_Flux_y"/>
    <fluxParallel name="S_Flux_front"/>
    <fluxParallel name="S_Flux_back"/>
    <fluxDensity name="V_Magnet">
      <comp dof="x" value="0"/>
      <comp dof="y" value="-1.5"/>
      <comp dof="z" value="0"/>
    </fluxDensity>  
  </bcsAndLoads> 
... 
~~~

A constant current density of $1 \frac{A}{mm^2}$ (or $10^6 \frac{A}{m^2}$) is applied to the coil, which is within the acceptable limits of current density for a copper wire ($<5\frac{A}{mm^2}$).

~~~
<magneticEdge>
...
  <coilList>
    <coil id="myCoil1">
      <source type="current" value="1e+06"/> 
      <part id="1">
        <regionList>
          <region name="V_Coil"/>
        </regionList>
	    <direction>
		  <analytic>
		    <comp dof="z" value="1"/>
		  </analytic>
		</direction>
        <wireCrossSection area="1"/>
        <resistance value="0"/> 
      </part>
    </coil>
  </coilList>
...
~~~

For the sake of simplicity in the mechanical PDE the spring at the bottom of the coil is modelled as concentrated stiffness element with stiffness $ 10^3 \frac{N}{m}$ in y-direction.

~~~
<mechanic subtype = "3d">
...
  <concentratedElem name="P_Spring" dof="y" stiffnessValue="1e+03"/>
...
~~~

The values of the coil current and spring stiffness were chosen such, that coil displacement would be large enough to show the impact of grid adaptation. 
In the smooth PDE the outer edges of the air domain are fixed. 
Along the symmetry axis additional fix boundary conditions in mechanic and smooth PDEs are necessary to ensure symmetry. 

### Coupling

The coupling between magnetic and mechanic PDE is done through Lorentz force acting on the coil. 
The Lorentz force density from magnetic PDE is applied at the coil as force density boundary condition in mechanic PDE.
~~~
<mechanic subtype="3d">
...
  <forceDensity name="V_Coil">
    <coupling pdeName="magneticEdge">
      <quantity name="magForceLorentzDensity"/>
    </coupling>
  </forceDensity> 
...
~~~

The coupling of the mechanical and smooth PDEs is a surface coupling at the air-coil interfaces. 
The interface displacements from the mechanic PDE are applied in the smooth PDE as boundary condition.

~~~
<smooth subType="3d">
...
  <displacement name="S_Smooth_c">
    <coupling pdeName="mechanic">
      <quantity name="mechDisplacement"/>
    </coupling>
  </displacement>
...
~~~

The couplings are iterative, so the number of iterations, the coupling quantities and stopping criteria are defined in couplingList section. 
The regions in which the geometry must be updated are also specified here.

~~~
<couplingList>
  <iterative>
    <convergence logging="yes" maxNumIters="20" stopOnDivergence="yes">
      <quantity name="mechDisplacement" value="1e-6" normType="rel"/>
      <quantity name="magForceLorentz" value="1e-6" normType="rel"/>
      <quantity name="smoothDisplacement" value="1e-6" normType="rel"/>
    </convergence>
    <geometryUpdate>
      <region name="V_Coil"/>
      <region name="V_Core"/>
      <region name="V_Air_up"/>
      <region name="V_Air_down"/>
    </geometryUpdate>
  </iterative>
</couplingList>
~~~

### Results

The resulting magnetic flux is shown in the figure below. 
One can observe the dominating magnetic field of the permanent magnet.

![image](flux_100A_static.png){: width="600px"}

The next figure shows the displacement of the coil and the displacements in the air domain. 
The smooth mesh deformation is visible.

![image](smooth_mesh_def.png){: width="600px"}


The displacement of the coil in y-direction with the smooth PDE and geometry update was compared to the same set-up [without geometry update or grid adaptation.](MagnetCoil_ref.xml) for different values of the coil current density between $0.01 \frac{A}{mm^2}$ and $2\frac{A}{mm^2}$. 
As visible in the plot below, for lower currents updating the mesh does not have a significant impact. 
For higher currents, however, the results with updated mesh are more accurate and show the non-linear current-displacement relation. 
The mesh smoothing PDE enhances the geometry update feature and allows for simulating larger displacements without remeshing. 

![image](edge_curve.png){: width="700px"}

In this example the element size is approximately $0.02m$. 
Without mesh smoothing the simulation with geometry update crashes for displacement larger than $0.0092m$. 
With mesh smoothing deformations up to $0.055m$ can be simulated. 

## Transient analysis

The second step in [MagnetCoil_smooth.xml](MagnetCoil_smooth.xml) is the transient analysis. 
Again, the same set-up without grid adaptation can be simulated with [MagnetCoil_ref.xml](MagnetCoil_ref.xml) for comparison.
In the transient analysis 1500 time steps with step size $\Delta t = 0.007s$ were computed.

~~~
<sequenceStep index="2">
  <analysis>
    <transient>
      <numSteps> 1500 </numSteps>
      <deltaT> 7e-03 </deltaT>
    </transient> 
  </analysis>
 ...
~~~

### Boundary conditions

The current denisty applied to the coil is now time-dependent and defined by a sine function with amplitude $2\frac{A}{mm^2}$ and frequency 5Hz. 
The frequency of the current was chosen as approximately two times the first eigenfrequency of the system(2.539Hz).

~~~
<magneticEdge>
...
  <coilList>
    <coil id="myCoil1">
      <source type="current" value="2e+06*sin(2*pi*5*t)"/> 
      <part id="1">
        <regionList>
          <region name="V_Coil"/>
        </regionList>
	    <direction>
		  <analytic>
		    <comp dof="z" value="1"/>
		  </analytic>
		</direction>
        <wireCrossSection area="1"/>
        <resistance value="0"/> 
      </part>
    </coil>
  </coilList>
...
~~~

To have the system reach a steady state a mechanical damping has been introduced as concentrated element. 
The chosen value of damping corresponds to the damping ratio $\zeta \approx 0.087$.

~~~
<mechanic subtype = "3d">
...
  <concentratedElem name="P_Spring" dof="y" stiffnessValue="1e+03" dampingValue="9"/>
...
~~~

Other boundary conditions are the same as in the static case.

### Results

The animation shows the coil oscillating after reaching the steady state. The smoothing of the mesh in air region is visible.

![image](smooth.gif){: width="600px"}

The first plot below shows the mean coil displacement in y-direction over the whole simulated time interval. 
Initially we can see the impact of first eigenfrequency of the system. Every second peak in the oscillation is visibly larger then others. As the system settles we see more regular oscillation at only the current frequency. 
With the chosen exctiation the coil displacements are large enough for the geometry update to have a visible impact. 
Especially in the initial part of the simulation the grid adaptation leads to higher amplitudes of the oscillation. 
This effect can also be observed in the steady state. 
The second plot shows the steady state oscillation of the coil in time interval 9.5s-10.5s in more detail. In the case with geometry update the equilibrium is shifted slightly away from zero. The oscillation amplitude in positive y-direction is smaller than the amplitude in negative direction. 

![image](transient_disp.png){: width="700px"}

![image](transient_disp_ss.png){: width="700px"}
