// Definition of constants

SetFactory("OpenCASCADE");

height=10;
excitationW=5;
receiverW=5;
pmlW=1;
elemSize = 0.05;  
interfaceW=1;
pos_middle_ex=2.5;
pos_middle_re=1.5;
fin_height=0.2;


// Bottom up modeling approach for structured mesh: Points → lines → surfaces → volumes → mesh

// Define basepoint for plate
Point(1) = {-excitationW-pmlW, 0 , 0, 1.0};
Point(2) = {-excitationW, 0 , 0, 1.0};
Point(3)={-pos_middle_ex, 0 , 0, 1.0};
Point(4) = {0, 0 , 0, 1.0};
Point(5) = {interfaceW, 0 , 0, 1.0};
Point(6) = {interfaceW+pos_middle_re, 0 , 0, 1.0};
Point(7) = {interfaceW+receiverW, 0 , 0, 1.0};
Point(8) = {interfaceW+receiverW+pmlW, 0 , 0, 1.0};


Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};


Extrude{ 0, height, 0 }{ Line{1,2,3,4,5,6,7}; }


Point(101)={0.05,0.625,0,1};
Point(102)={0.95,0.175,0,1};
Line(100) = {101, 102};

Point(201)={0.05,0.625+1.25,0,1};
Point(202)={0.95,0.175+1.25,0,1};
Line(200) = {201, 202};

Point(301)={0.05,0.625+2*1.25,0,1};
Point(302)={0.95,0.175+2*1.25,0,1};
Line(300) = {301, 302};

Point(401)={0.05,0.625+3*1.25,0,1};
Point(402)={0.95,0.175+3*1.25,0,1};
Line(400) = {401, 402};

Point(501)={0.05,0.625+4*1.25,0,1};
Point(502)={0.95,0.175+4*1.25,0,1};
Line(500) = {501, 502};

Point(601)={0.05,0.625+5*1.25,0,1};
Point(602)={0.95,0.175+5*1.25,0,1};
Line(600) = {601, 602};

Point(701)={0.05,0.625+6*1.25,0,1};
Point(702)={0.95,0.175+6*1.25,0,1};
Line(700) = {701, 702};

Point(801)={0.05,0.625+7*1.25,0,1};
Point(802)={0.95,0.175+7*1.25,0,1};
Line(800) = {801, 802};


Extrude{ 0, fin_height, 0 }{ Line{100,200,300,400,500,600,700,800}; }

Curve Loop(1000) = {13, 16, 15, 4};
Plane Surface(110) = {1000};

BooleanDifference{ Surface{110}; Delete; }{ Surface{8,9,10,11,12,13,14,15};  }

// Get rid of multiple entities and current states
Coherence;


// Generate regions to address later in CFS++
Physical Surface("s_air_ex") = {2,3};
Physical Surface("s_pml_ex") = {1};
Physical Surface("s_air_if") = {110}; 
Physical Surface("s_air_re") = {5,6}; 
Physical Surface("s_pml_re") = {7}; 
Physical Surface("s_if") = {8, 9, 10, 11, 12,13,14,15}; 

Physical Curve("c_top")={10,12,14,18,20,22,826};
Physical Curve("c_bottom")={1,2,3,825,5,6,7};
Physical Curve("c_rhs")={9};
Physical Curve("c_if_in")={13};
Physical Curve("c_if_out")={15};
Physical Curve("c_middle_ex")={11};
Physical Curve("c_middle_re")={17};
Physical Curve("c_re_end")={19};

// Structured meshing and recombine triangles to quadrilaterals
Transfinite Surface {2,1,3,7,5,6};
Recombine Surface '*';

Transfinite Line {9,8,11,13,15,17,19,21} = ((height)/elemSize) Using Progression 1;
Transfinite Line {1,10,7,22} = ((pmlW)/elemSize) Using Progression 1;
Transfinite Line {2,12} = ((excitationW-pos_middle_ex)/elemSize) Using Progression 1;
Transfinite Line {3,14} = (pos_middle_ex/elemSize) Using Progression 1;
Transfinite Line {5,18} = (pos_middle_re/elemSize) Using Progression 1;
Transfinite Line {6,20} = ((receiverW-pos_middle_re)/elemSize) Using Progression 1;
Transfinite Line {825,826} = ((interfaceW)/elemSize) Using Progression 1;
Transfinite Line {100,200,300,400,500,600,700,800,803,806,809,812,815,818,821,824} = ((1.04)/elemSize) Using Progression 1;
Transfinite Line {801,802,804,805,807,808,810,811,813,814,816,817,819,820,822,823} = ((fin_height)/elemSize) Using Progression 1;

// Perform 2D meshing with 1st order elements
Mesh.ElementOrder = 1;
Mesh 2;