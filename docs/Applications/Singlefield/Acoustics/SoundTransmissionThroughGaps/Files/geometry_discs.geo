// Definition of constants

SetFactory("OpenCASCADE");

height=10;
excitationW=5;
receiverW=5;
pmlW=1;
elemSize = 0.05;  
interfaceW=1;
pos_middle_ex=2.5;
pos_middle_re=1.5;

// Bottom up modeling approach for structured mesh: Points → lines → surfaces → volumes → mesh

// Define basepoint for plate
Point(1) = {-excitationW-pmlW, 0 , 0, 1.0};
Point(2) = {-excitationW, 0 , 0, 1.0};
Point(3)={-pos_middle_ex, 0 , 0, 1.0};
Point(4) = {0, 0 , 0, 1.0};
Point(5) = {interfaceW, 0 , 0, 1.0};
Point(6) = {interfaceW+pos_middle_re, 0 , 0, 1.0};
Point(7) = {interfaceW+receiverW, 0 , 0, 1.0};
Point(8) = {interfaceW+receiverW+pmlW, 0 , 0, 1.0};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};


Extrude{ 0, height, 0 }{ Line{1,2,3,4,5,6,7}; }

Disk(101) = {0.5, 1, 0, 0.45, 0.45};
Disk(102) = {0.5, 3, 0, 0.45, 0.45};
Disk(103) = {0.5, 5, 0, 0.45, 0.45};
Disk(104) = {0.5, 7, 0, 0.45, 0.45};
Disk(105) = {0.5, 9, 0, 0.45, 0.45};

//+
Curve Loop(100) = {13, 16, 15, 4};
//+
Plane Surface(110) = {100};

BooleanDifference{ Surface{110}; Delete; }{ Surface{101,102,103,104};  }

// Get rid of multiple entities and current states
Coherence;

// Generate regions to address later in CFS++
Physical Surface("s_air_ex") = {2,3};
Physical Surface("s_pml_ex") = {1};
Physical Surface("s_air_if") = {106}; 
Physical Surface("s_air_re") = {5,6}; 
Physical Surface("s_pml_re") = {7}; 
Physical Surface("s_if") = {101, 102, 103, 104, 105}; 

Physical Curve("c_top")={10,12,14,18,20,22,28};
Physical Curve("c_bottom")={1,2,3,27,5,6,7};
Physical Curve("c_rhs")={9};
Physical Curve("c_if_in")={13};
Physical Curve("c_if_out")={15};
Physical Curve("c_middle_ex")={11};
Physical Curve("c_middle_re")={17};
Physical Curve("c_re_end")={19};

// Structured meshing and recombine triangles to quadrilaterals
Transfinite Surface {2,1,3,7,5,6};
Recombine Surface '*';

Transfinite Line {9,8,11,13,15,17,19,21} = ((height)/elemSize) Using Progression 1;
Transfinite Line {1,10,7,22} = ((pmlW)/elemSize) Using Progression 1;
Transfinite Line {2,12} = ((excitationW-pos_middle_ex)/elemSize) Using Progression 1;
Transfinite Line {3,14} = (pos_middle_ex/elemSize) Using Progression 1;
Transfinite Line {5,18} = (pos_middle_re/elemSize) Using Progression 1;
Transfinite Line {6,20} = ((receiverW-pos_middle_re)/elemSize) Using Progression 1;
Transfinite Line {27,28} = ((interfaceW)/elemSize) Using Progression 1;
Transfinite Curve {23,24,25,26,29} = ((2.8)/elemSize) Using Progression 1;

// Perform 2D meshing with 1st order elements
Mesh.ElementOrder = 1;
Mesh 2;