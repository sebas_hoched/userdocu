
import pandas as pd
import numpy as np





def calculate_power(angle_str, simulation_str, freq_num):
    r"""Calculation of Power along Sensor Array
    
    Function for reading the data from the sensor arrays, subtracting the incoming from the entire data and 
    calculating the resulting power along the line of the sensor array.
    
    Parameters
    ----------
    angle_str : string
        A string, providing the information about the simulated angle
    simulation_str : string
        A string, providing the information about the performed simulation
    freq_num : int
        Number of simulated frequencies
   
    Returns
    -------
    df : pandas.DataFrame
        Dataframe containing all the Values necessary for calculating the reflected power
    Power : numpy.ndarray
        Reflected Power at each simulated frequency
    """
    
    Power=np.zeros((freq_num, 1))
    df = pd.DataFrame()

    # for loop over all simulated frequencies
    for i in range(1, freq_num+1):  
        # File Path for Pressure Array for the Full System !!!CHANGE PATH FOR NEW SIMULATION!!!
        file_path = f'./Arrays/'+simulation_str+'_'+angle_str+'/PressureArray-'+str(i)
        column_names_pres = ['ID', 'x', 'y','Pres_real','Pres_imag','xloc','yloc']
        df_pres = pd.read_csv(file_path, sep='\t', names=column_names_pres) # store in dataframe
        df_pres['Pres_comp']= df_pres['Pres_real']+1j*df_pres['Pres_imag'] # make complex pressure value

        # File Path for Velocity Array for the Full System !!!CHANGE PATH FOR NEW SIMULATION!!!
        file_path = f'./Arrays/'+simulation_str+'_'+angle_str+'/VelocityArray-'+str(i)
        column_names_vel = ['ID', 'x', 'y','xVel_real','yVel_real','xVel_imag','yVel_imag','xloc','yloc']
        df_vel = pd.read_csv(file_path, sep='\t', names=column_names_vel) # store in dataframe
        df_vel['Norm_vel_comp']=df_vel['xVel_real']+1j*df_vel['xVel_imag'] # make complex normal velocity value

        # File Path for Pressure Array for the Incoming System
        file_path = f'./Arrays/Inc_'+angle_str+'/PressureArray-'+str(i)
        column_names_pres = ['ID', 'x', 'y','Pres_real','Pres_imag','xloc','yloc']
        df_pres_rep = pd.read_csv(file_path, sep='\t', names=column_names_pres) # store in dataframe
        df_pres_rep['Pres_comp']= df_pres_rep['Pres_real']+1j*df_pres_rep['Pres_imag'] # make complex pressure value

        # File Path for Velocity Array for the Incoming System
        file_path = f'./Arrays/Inc_'+angle_str+'/VelocityArray-'+str(i)
        column_names_vel = ['ID', 'x', 'y','xVel_real','yVel_real','xVel_imag','yVel_imag','xloc','yloc']
        df_vel_rep = pd.read_csv(file_path, sep='\t', names=column_names_vel) # store in dataframe
        df_vel_rep['Norm_vel_comp']=df_vel_rep['xVel_real']+1j*df_vel_rep['xVel_imag'] # make complex normal velocity value

        # for the first frequency the element ID and position are put in the final dataframe
        if i==1:
            df['ID']=df_vel['ID']
            df['x']=df_vel['x']
            df['y']=df_vel['y']

        # subtracting the incoming pressure from the pressure of the entire system
        df['Pres_comb'+str(i)]=df_pres['Pres_comp']-df_pres_rep['Pres_comp']

        # subtracting the incoming velocity from the velocity of the entire system
        df['Norm_vel_comb'+str(i)]=df_vel['Norm_vel_comp']-df_vel_rep['Norm_vel_comp']

        # calculating the intensity with the resulting pressure and velocity
        df['Int_norm'+str(i)]=df['Pres_comb'+str(i)]*np.conjugate(df['Norm_vel_comb'+str(i)])
        df['Int_norm_real'+str(i)]=df['Int_norm'+str(i)].apply(lambda x: np.real(x))

        # calculate the power with the intensity
        Power[i-1]=abs(df['Int_norm_real'+str(i)]).sum()*0.05


    return [df,Power]


def read_inc_power(angle_str):
    r"""Reading and Storing the Incoming Power
    
    Function for reading and storing the incoming power at all simulated frequencies, calculated with openCFS 
    and stored in a textfile. Additionally the Frequency for each simulation is read and stored from the same
    file.
    
    Parameters
    ----------
    angle_str : string
        A string, providing the information about the simulated angle
   
    Returns
    -------
    Power_inc : list
        Incoming Power at each simulated frequency
    frequency : list
        List of frequencies where the simulation was carried out
    """

    # File Path for Power of the incoming Acoustic Field
    filepath=f'./history/Inc_'+angle_str+'-acouPower-surfRegion-c_rightside.hist'
    with open(filepath, 'r') as file:
        # Read all lines from the file
        lines = file.readlines()

        # Initialize empty lists for incoming power and frequency
        Power_inc = []
        frequency = []

        # Iterate over each line in the file
        for line in lines:
 
            columns = line.split()
            if columns[0] != '#': 
                Power_inc.append(float(columns[1])*np.cos(np.deg2rad(float(columns[2]))))  # Extract real value of Power
                frequency.append(float(columns[0])) # Extract frequency

    return [Power_inc,frequency]

def read_trans_power(angle_str, simulation_str):
    
    r"""Reading and Storing the Transmitted Power
    
    Function for reading and storing the trasmitted power at all simulated frequencies, calculated with openCFS 
    and stored in a textfile. Additionally the Frequency for each simulation is read and stored from the same
    file.
    
    Parameters
    ----------
    angle_str : string
        A string, providing the information about the simulated angle
    simulation_str : string
        A string, providing the information about the performed simulation

    Returns
    -------
    Power_trans : list
        Transmitted Power at each simulated frequency
    frequency : list
        List of frequencies where the simulation was carried out
    """
    
    # File Path for Power transmitted through the Interface !!!CHANGE PATH FOR NEW SIMULATION!!!
    filepath=f'./history/'+simulation_str+'_'+angle_str+'-acouPower-surfRegion-c_if_out.hist'
    with open(filepath, 'r') as file:
        # Read all lines from the file
        lines = file.readlines()

        # Initialize empty lists for transmitted power
        Power_trans = []
        frequency = []
        
        # Iterate over each line in the file
        for line in lines:

            columns = line.split()
            if columns[0] != '#':
                Power_trans.append(float(columns[1])*np.cos(np.deg2rad(float(columns[2]))))  # Extract real value of Power
                frequency.append(float(columns[0])) # Extract frequency
                
    return [Power_trans]
