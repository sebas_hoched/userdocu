Installing Pre-Processors for openCFS
=====================================

Like many FE codes openCFS relies on external tools to provide meshes for the analysis.
openCFS can read various mesh formats. A few selected software choices are given in the following.

## Gmsh

[Gmsh](https://gmsh.info/) is an open source 3D finite element mesh generator with a built-in CAD engine and post-processor.
It is available for all common operating systems: [downloaded](https://gmsh.info/#Download) or install it directly via your package manager on Linux. 

## cubit / trelis

Coreform Cubit (formerly trelis) is a powerful mesh generator including geometry modelling and import features.
It is available for Windows, MacOS and Linux.
There is a free version available: [Coreform Cubit Learn](https://coreform.com/products/coreform-cubit/free-meshing-software/).

To get started:
1. [download](https://coreform.com/products/downloads/#Coreform-Cubit) Coreform-Cubit for your platform 
2. install the software
3. enter your licensing information on the first start.
   For [Coreform Cubit Learn](https://coreform.com/products/coreform-cubit/free-meshing-software/) you will obtain a product-key for a node-locked license after you register ([detailed activation instructions](https://coreform.com/support/activation/cubit/coreform-cubit-learn-activation/))