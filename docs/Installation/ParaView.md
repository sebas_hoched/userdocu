Installing ParaView and activating the CFSReader Plugin
=======================================================

In order to read openCFS native HDF5 data format (`*.cfs` files) with paraview you need our **CFSReader plugin**.
The plugin has been included into paraview (from version 5.12) and needs to be activated before it can be used.

ParaView
--------

You can build ParaView from source or install an [official ParaView release](https://www.paraview.org/download/) for your operation system.

**Note:** The plugin is included in ParaView from version 5.12. Currently 5.12 is labelled as "release candidate", see the [preliminary release notes](https://gitlab.kitware.com/paraview/paraview/-/blob/master/Documentation/release/ParaView-5.12.0.md). 

CFSReader Plugin
----------------

In order to read openCFS result files (`*.cfs` files) with paraview, one must activate the **CFSReader plugin**.

Follow these steps:

  * Open ParaView and in the menu bar click **Tools > Manage Plugins**

  * Select **CFSReader** and click the **Load Selected** button (you can also activate the **Auto Load** check box to load the plugin on every paraview start)

  * Now **.cfs** files can be opened and visualized using ParaView










